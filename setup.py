from setuptools import setup

install_requires = [
    "redis==2.10.6",
]

# install_requires versions are handled by Python in runtime only!
# if you update this also update the BitBake recipe of the app using this package
# something like RDEPENDS_${PN} += "python3-redis (>= 2.10.6)"

setup(
    name='messageqclient',
    version='1.0.0',
    description=
    'Redis client to support the Local-Agent short-term storage requirements',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com/poky_noty/messageq-client',
    license='MIT License',
    packages=['messageqclient', 'messageqclient.redis'],
    install_requires=install_requires,
    classifiers=[
        'Development Status :: 1 - Development/Testing',
        'Environment :: Module',
        'Programming Language :: Python :: 3.4',
        'Topic :: Storage :: Memory :: Key-Value :: Pub/Sub :: Listener :: Topic',
    ],
    #entry_points={
    #    'console_scripts': [
    #        'messageqclient=messageqclient.__main__:main'
    #    ]
    #},
    zip_safe=True)
