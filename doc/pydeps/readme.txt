on target:

pydeps ./usr/lib/python3.7/site-packages/messageqclient --show-dot --noshow --pylib

on host:

copy over the result to host as messageqclient.dot

dot -Tsvg messageqclient.dot -o messageqclient.svg
