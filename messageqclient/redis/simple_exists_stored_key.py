from . import *


def exists_stored_key(client, key="dummy"):
    #logger.debug("Ready to Check if Key Exists %s", key)
    response = client.exists(key)
    #logger.debug("Successful Key Exists Response is %s", response)

    return response


def get_keys_list(client, pattern):
    keys_list = client.keys(pattern)

    keys = [k.decode() for k in keys_list]

    return keys
