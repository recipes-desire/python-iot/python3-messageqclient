import redis

from . import *


def establish_single_connection(logger, host, port, db):
    logger.debug("Trying to establish a network connection with Redis")
    client = redis.StrictRedis(host=host, port=port, db=db)
    logger.debug("Successful Connection & Client is %s", client)

    # The test
    ping = client.ping()
    logger.debug("Successful Test & Ping is %s", ping)
    logger.debug("Redis Server Info is ['Version']=%s",
                 client.info().get("redis_version"))

    return client


def establish_pool_connection():
    pass
