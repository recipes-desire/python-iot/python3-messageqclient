def left_add_values(redis_client, key, values_arr):
    redis_client.lpush(key, values_arr)


def list_elements_backwards(redis_client, key, start=0, stop=-1):
    redis_client.lrange(key, start, stop)


def list_elements_forwards(redis_client, key, start=0, stop=100):
    redis_client.lrange(key, start, stop)


def append_many_values_left(redis_client, key, values_list):
    # Important use the * to unpack the array
    redis_client.lpush(key, *values_list)


def append_many_values_right(redis_client, key, values_list):
    redis_client.rpush(key, *values_list)
    #redis_client.lpush( key, *[1,2,3,4,5,6] )


def get_all_values_from_set(redis_client, key):
    return redis_client.lrange(key, 0, -1)
