from .redis.simple_connection import establish_single_connection


def main():
    establish_single_connection()
