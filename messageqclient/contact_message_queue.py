from . import *
from .redis import *

from .constants import KeyNames

import os

TYPE = "REDIS"

client = None
logger = None


def check_datastore_availability(_logger, host, port, db):
    global logger
    logger = _logger

    logger.debug("Ready to check .... Datastore connection")

    if TYPE.upper() == "REDIS":
        global client

        logger.debug("Saving data to REDIS store ... Is supported !")

        from .redis.simple_test import is_redis_available_for_data

        client = is_redis_available_for_data(logger, host, port, db)

        return True


def check_key_existence(key):
    if TYPE.upper() == "REDIS":
        global client

        from .redis.simple_exists_stored_key import exists_stored_key
        response = exists_stored_key(client=client, key=key)

        return response


def get_all_values_from_set(key):
    if TYPE.upper() == "REDIS":
        global client

        from .redis.handle_list_of_values import get_all_values_from_set
        response = get_all_values_from_set(client, key)

        return response


def append_end(key, values):
    if TYPE.upper() == "REDIS":
        global client

        logger.debug("1 --> %s\n%s", key, values)
        append_many_values_right(client, key, values)


def append_start(key, values):
    if TYPE.upper() == "REDIS":
        global client

        logger.debug("2 --> %s\n%s", key, values)
        append_many_values_left(client, key, values)


def delete_data_from_db(logger):
    if TYPE.upper() == "REDIS":
        global client

        logger.debug("Ready to Delete ALL Keys from Current DB !")
        response = delete_all_keys_current(client)

        return response


def save_open_ports_for_server(server_id, ports_list):
    if TYPE.upper() == "REDIS":
        global client

        # Check Parent Key Exists
        parent_key = KeyNames().openports
        server_key = parent_key + ":" + server_id

        #logger.debug( " --> %s .... %s", server_key, ports_list )

        # Save (key)Server-ID : (value)Open-Ports
        response = set_single_key_value(client,
                                        key=server_key,
                                        value=ports_list)

        if not response:
            # Failure .. save open-ports to Datastore
            logger.error("Failed to store open-ports for server ... %s",
                         server_id)


"""
What ports on a Linux server are open ... get a list of 
"""


def get_open_ports_for_server(server_id):
    if TYPE.upper() == "REDIS":
        global client

        #logger.debug("Ready to get a list of open ports on linux server ... %s", server_id)

        key = KeyNames().openports + ":" + server_id

        if not check_key_existence(key):
            #raise Exception( "Not Found Key ... {}".format( key ) )
            logger.debug(
                "Not Found Key for Open-Ports List in Datastore ... First Time Run ?"
            )
            return

        # Key exists ... find all elements
        #return get_all_members_of_set( client, key )

        response = get_single_key_value(client, key)
        if not response:
            # Failure retrieving values from datastore
            logger.debug(
                "Querying Datastore for Open-Ports of Server %s .... Empty ResultSet",
                key)

            return

        #logger.debug("Get VALUES response ... %s", response )
        return response


def my_keys_list(pattern="*"):
    if TYPE.upper() == "REDIS":
        global client

        return get_keys_list(client, pattern)


def get_stored_hosts_up():
    if TYPE.upper() == "REDIS":
        global client

        h = KeyNames().hostsup

        return get_single_key_value(client, key=h)
